//
//  Songs+CoreDataProperties.swift
//  MusicApp
//
//  Created by Admin on 7.02.19.
//  Copyright © 2019 Admin. All rights reserved.
//
//

import Foundation
import CoreData


extension Songs {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Songs> {
        return NSFetchRequest<Songs>(entityName: "Songs")
    }

    @NSManaged public var songName: String?
    @NSManaged public var relationshipToPlaylist: NSSet?

}

// MARK: Generated accessors for relationshipToPlaylist
extension Songs {

    @objc(addRelationshipToPlaylistObject:)
    @NSManaged public func addToRelationshipToPlaylist(_ value: Playlists)

    @objc(removeRelationshipToPlaylistObject:)
    @NSManaged public func removeFromRelationshipToPlaylist(_ value: Playlists)

    @objc(addRelationshipToPlaylist:)
    @NSManaged public func addToRelationshipToPlaylist(_ values: NSSet)

    @objc(removeRelationshipToPlaylist:)
    @NSManaged public func removeFromRelationshipToPlaylist(_ values: NSSet)

}
