//
//  Playlists+CoreDataProperties.swift
//  MusicApp
//
//  Created by Admin on 7.02.19.
//  Copyright © 2019 Admin. All rights reserved.
//
//

import Foundation
import CoreData


extension Playlists {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Playlists> {
        return NSFetchRequest<Playlists>(entityName: "Playlists")
    }

    @NSManaged public var playlistName: String?
    @NSManaged public var relationshipToSongs: NSSet?

}

// MARK: Generated accessors for relationshipToSongs
extension Playlists {

    @objc(addRelationshipToSongsObject:)
    @NSManaged public func addToRelationshipToSongs(_ value: Songs)

    @objc(removeRelationshipToSongsObject:)
    @NSManaged public func removeFromRelationshipToSongs(_ value: Songs)

    @objc(addRelationshipToSongs:)
    @NSManaged public func addToRelationshipToSongs(_ values: NSSet)

    @objc(removeRelationshipToSongs:)
    @NSManaged public func removeFromRelationshipToSongs(_ values: NSSet)

}
