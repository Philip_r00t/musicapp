//
//  CustomAllertBox.swift
//  MusicApp
//
//  Created by Admin on 5.02.19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension UIViewController {

    //AllertBox for saving items to CoreData
    
    func saveDialog(cellIndexPath: IndexPath, completionHandler: @escaping (Bool) -> Void){
        var textPlaceholder: String!
        var alertboxTitle: String!
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        
        switch  cellIndexPath.row {
        case 0:
            textPlaceholder = "Enter Song Name"
            alertboxTitle = "Add New Song"
        case 1:
            textPlaceholder = "Enter Playlist Name"
            alertboxTitle = "Add New Playlist"
        default:
            textPlaceholder = "Invalid Selection"
        }
        
        let alertController = UIAlertController(title: alertboxTitle, message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = textPlaceholder
        }
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            if firstTextField.text != "" {
                
                switch  cellIndexPath.row {
                case 0:
                    let song = Songs(context: context)
                    song.songName = firstTextField.text
                case 1:
                    let playlist = Playlists(context: context)
                    playlist.playlistName = firstTextField.text
                    
                    var defaultSongs = [Songs]()
                    for i in 0...9 {
                        let song = Songs(context: context)
                        song.songName = "Song\(i+1)"
                        song.addToRelationshipToPlaylist(playlist)
                        defaultSongs.append(song)
                    }
                default:
                    textPlaceholder = "Invalid Selection"
                }
                
                
                do {
                    try context.save()
                } catch {
                    print("Failed saving")
                }
                
            }
           
            completionHandler(true)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}
