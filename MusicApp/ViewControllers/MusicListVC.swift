//
//  List.swift
//  MusicApp
//
//  Created by Admin on 3.02.19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import CoreData

class MusicListVC: UIViewController, UITableViewDataSource, UITableViewDelegate, ExpandableHeaderViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var playlists = [Playlist]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //table view DataSource
        playlists.removeAll()
        fetchPlaylists()
        fetchSongs()
        
        // if playlist array is empty hide tableView and show add button.
        if playlists.isEmpty{
            tableView.isHidden = true
        }else{
            tableView.isHidden = false
        }
    }
    
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return playlists.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if playlists[section].expanded{
            return playlists[section].songs.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SongCell", for: indexPath) as! SongInfoCell
        cell.songTitle.text = playlists[indexPath.section].songs[indexPath.row]
        cell.songPlaylist.text = "Playlist: " + playlists[indexPath.section].songPlaylist[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animate(withDuration: 0.3, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
        },completion: { finished in
            UIView.animate(withDuration: 0.1, animations: {
                cell.layer.transform = CATransform3DMakeScale(1,1,1)
            })
        })
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = ExpandableHeaderView()
        header.customInit(title: playlists[section].name, section: section, delegate: self)
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // toggle section if user tap on it
    func toggleSection(header: ExpandableHeaderView, section: Int) {
        for i in 0 ..< playlists.count{
            if i == section {
                playlists[section].expanded = !playlists[section].expanded
            }else{
                playlists[i].expanded = false
            }
        }
        self.tableView.reloadData()
    }
    
    // Methods for fetching songs and playlists from Core Data
    func fetchPlaylists(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Playlists")
        
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [Playlists] {
                let playlistName = (data.value(forKey: "playlistName") as! String)
                
                let songsArr = data.relationshipToSongs?.allObjects as! [Songs]
                var songNames = [String]()
                var songPlaylist = [String]()
                for i in songsArr{
                    guard let songName = i.songName else {
                        return
                    }
                    guard let playlistName = data.playlistName else {
                        return
                    }
                    songNames.append(songName)
                    songPlaylist.append(playlistName)
                }
                songNames = songNames.sorted {$0.localizedStandardCompare($1) == .orderedAscending}
                playlists.append(Playlist(name: playlistName, songs: songNames, expanded: false, songPlaylist: songPlaylist))
            }
        } catch {
            print("Failed")
        }
        self.tableView.reloadData()
        
    }
    
    
    func fetchSongs(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Songs")
        
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            var songArray = [String]()
            var playlistArray = [String]()
            for data in result as! [Songs] {
                songArray.append(data.value(forKey: "songName") as! String)
                
                let relatedPlaylistName = data.relationshipToPlaylist?.value(forKey: "playlistName") as! Set<String>
                let arr = [String](relatedPlaylistName)
                if !arr.isEmpty {
                    playlistArray.append(arr[0])
                }else{
                    playlistArray.append("Not In Playlist")
                }
                
            }
            if !songArray.isEmpty {
                // if songArray is not empty, create playlist with songs from array
                playlists.append(Playlist(name: "Others", songs: songArray, expanded: false, songPlaylist: playlistArray))
            }
        } catch {
            print("Failed")
        }
        playlists = playlists.sorted { $0.name < $1.name }
        self.tableView.reloadData()
    }
    
    
}

