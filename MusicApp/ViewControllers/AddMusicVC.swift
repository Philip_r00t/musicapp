//
//  AddMusicVC.swift
//  MusicApp
//
//  Created by Admin on 5.02.19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import CoreData


class AddMusicVC: UITableViewController {
    let tableCellsNames = ["Add Song", "Add Playlist"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableCellsNames.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> AddMusicCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addMusicCell", for: indexPath) as! AddMusicCell

        cell.cellLabel.text = tableCellsNames[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Show dialog box, input name of item and save
        saveDialog(cellIndexPath: indexPath, completionHandler: { success in
            self.navigationController?.popToRootViewController(animated: true)
        })
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}
