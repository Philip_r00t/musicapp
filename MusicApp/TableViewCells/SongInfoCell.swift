//
//  SongInfoCell.swift
//  MusicApp
//
//  Created by Admin on 3.02.19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SongInfoCell: UITableViewCell {
    
    
    
    
    
    @IBOutlet weak var musicArtwork: UIImageView!
    
    @IBOutlet weak var songTitle: UILabel!
    
    @IBOutlet weak var songPlaylist: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
