//
//  AddMusicCell.swift
//  MusicApp
//
//  Created by Admin on 5.02.19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AddMusicCell: UITableViewCell {

    @IBOutlet weak var addIcon: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
