//
//  Playlist.swift
//  MusicApp
//
//  Created by Admin on 4.02.19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

struct Playlist {
    var name: String!
    var songs: [String]!
    var songPlaylist: [String]!
    var expanded: Bool!
    
    init(name:String, songs:[String], expanded:Bool, songPlaylist:[String]) {
        self.name = name
        self.songs = songs
        self.expanded = expanded
        self.songPlaylist = songPlaylist
    }
}
